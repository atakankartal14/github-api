# Hub

Hub is a Github client written in Swift by using VIPER architecture. While Presenters & Interactors & Workers of modules are 100% tested by Unit Tests, 73% test coverage for total project is achieved.

### You can;

 * Login to your Github account
 * See your profile
 * Search for other Github users and even follow them!

## Installation

Clone the repository to use Hub.

```bash
git clone https://atakankartal14@bitbucket.org/atakankartal14/github-api.git
```

## Targets

- Test: Unit tests written without a 3rd party library
- Hub: Project target

### Project Configuration

* `podfile` containing 
   * Moya, Reusable, KingFisher and some others

### Networking

* `Moya` for network layer


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
