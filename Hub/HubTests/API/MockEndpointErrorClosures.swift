//
//  MockEndpointErrorClosures.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Moya
@testable import Hub

class MockEndpointErrorClosures {

    static var oauthErrorClosure = { (target: OAuthTarget<TestEnvironment>) -> Endpoint in
        return Endpoint(url: URL(target: target).absoluteString,
                        sampleResponseClosure: { .networkResponse(400, .init()) },
                        method: target.method,
                        task: target.task,
                        httpHeaderFields: target.headers)
    }

    static var searchErrorClosure = { (target: SearchTarget<TestEnvironment>) -> Endpoint in
        return Endpoint(url: URL(target: target).absoluteString,
                        sampleResponseClosure: { .networkResponse(400, .init()) },
                        method: target.method,
                        task: target.task,
                        httpHeaderFields: target.headers)
    }

    static var userErrorClosure = { (target: UserTarget<TestEnvironment>) -> Endpoint in
        return Endpoint(url: URL(target: target).absoluteString,
                        sampleResponseClosure: { .networkResponse(400, .init()) },
                        method: target.method,
                        task: target.task,
                        httpHeaderFields: target.headers)
    }
}
