//
//  MockAPI.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Moya
@testable import Hub

class MockAPI: APIType {

    typealias E = TestEnvironment

    // MARK: - OAuth Mock Providers
    static var oauthProviderSuccess = MoyaProvider<OAuthTarget<E>>.init(stubClosure: MoyaProvider.immediatelyStub,
                                                                          plugins: TargetPlugins.plugins)
    
    static var oauthProviderFail = MoyaProvider<OAuthTarget<E>>.init(endpointClosure: MockEndpointErrorClosures.oauthErrorClosure,
                                                                       stubClosure: MoyaProvider.immediatelyStub,
                                                                       plugins: TargetPlugins.plugins)

    // MARK: - Search Mock Providers
    static var searchProviderSuccess = MoyaProvider<SearchTarget<E>>.init(stubClosure: MoyaProvider.immediatelyStub,
                                                                          plugins: TargetPlugins.plugins)

    static var searchProviderNeverEnds = MoyaProvider<SearchTarget<E>>.init(stubClosure: MoyaProvider.neverStub,
                                                                              plugins: TargetPlugins.plugins)
    
    static var searchProviderFail = MoyaProvider<SearchTarget<E>>.init(endpointClosure: MockEndpointErrorClosures.searchErrorClosure,
                                                                       stubClosure: MoyaProvider.immediatelyStub,
                                                                       plugins: TargetPlugins.plugins)

    // MARK: - User Mock Providers
    static var userProviderSuccess = MoyaProvider<UserTarget<E>>.init(stubClosure: MoyaProvider.immediatelyStub,
                                                                        plugins: TargetPlugins.plugins)
    
    static var userProviderFail = MoyaProvider<UserTarget<E>>.init(endpointClosure: MockEndpointErrorClosures.userErrorClosure,
                                                                     stubClosure: MoyaProvider.immediatelyStub,
                                                                     plugins: TargetPlugins.plugins)
}
