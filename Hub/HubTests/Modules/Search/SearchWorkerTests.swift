//
//  SearchWorkerTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 6.09.2021.
//

import XCTest
@testable import Hub

class SearchWorkerTests: XCTestCase {

    var sutSuccess: SearchWorker!
    var sutFail: SearchWorker!
    var sutNeverResponds: SearchWorker!
    var interactor: MockSearchInteractor!

    override func setUp() {
        sutSuccess = .init(searchProvider: MockAPI.searchProviderSuccess, userProvider: MockAPI.userProviderSuccess)
        sutFail = .init(searchProvider: MockAPI.searchProviderFail, userProvider: MockAPI.userProviderFail)
        sutNeverResponds = .init(searchProvider: MockAPI.searchProviderNeverEnds, userProvider: MockAPI.userProviderFail)
        interactor = .init()
        sutSuccess.interactor = interactor
        sutFail.interactor = interactor
        sutNeverResponds.interactor = interactor
        super.setUp()
    }

    override func tearDown() {
        sutSuccess = nil
        sutFail = nil
        interactor = nil
        super.tearDown()
    }

    // MARK: - Search
    func testSetsLoadingWhenSearchStarted() {
        sutNeverResponds.search(for: "")
        XCTAssertTrue(sutNeverResponds.isLoading())
    }

    func testSendsErrorToInteractorWhenNetworkingFailedForSearch() {
        sutFail.search(for: "")
        XCTAssertEqual(interactor.errorMessage, "Error!")
    }

    func testSearchUpdatesItemCountOnSuccess() {
        sutSuccess.search(for: "")
        XCTAssertEqual(interactor.searchItems.count, 1)
    }

    func testSearchUpdatesTotalCountOnSuccess() {
        sutSuccess.search(for: "")
        XCTAssertEqual(interactor.totalCount, 5)
    }

    // MARK: - Fetch Next Page
    func testFetchNextPageUpdatesItemCountOnSuccess() {
        sutSuccess.fetchNextPage()
        XCTAssertEqual(interactor.searchItems.count, 1)
    }

    // MARK: - Follow
    func testSendsErrorToInteractorWhenNetworkingFailedForFollow() {
        sutFail.follow(username: "")
        XCTAssertEqual(interactor.errorMessage, "Error!")
    }

    func testFollowSucceeded() {
        sutSuccess.follow(username: "")
        XCTAssertTrue(interactor.followSucceded)
    }
}
