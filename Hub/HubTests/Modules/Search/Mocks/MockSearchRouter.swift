//
//  MockSearchRouter.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockSearchRouter: SearchRouterInterface {

    var alertMessage = ""
    var didPush = false
    
    func showAlert(message: String) {
        alertMessage = message
    }
    
    func showProfile() {
        didPush = true
    }
    
}
