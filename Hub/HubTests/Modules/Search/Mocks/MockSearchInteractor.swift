//
//  MockSearchInteractor.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockSearchInteractor: SearchInteractorInterface, SearchWorkerOutputProtocol {

    var presenter: SearchInteractorOutputProtocol?
    var isFetchUserProcessing = false
    var isFollowProcessing = false
    var isFetchNextPageProcessing = false
    var shouldContinue = true
    var shouldFail = false
    var canFetchNext = true
    var loading = false
    var searchItems = [UserResponse]()
    var totalCount = 0
    var followSucceded = false
    var errorMessage = ""

    func searchUsers(for query: String) {
        isFetchUserProcessing = true
        guard shouldContinue else { return }
        if shouldFail {
            presenter?.didFail(with: MockSearchError.search.rawValue)
        } else {
            presenter?.searchDidSucceed()
        }
    }
    
    func followUser(username: String?) {
        isFollowProcessing = true
        guard shouldContinue else { return }
        if shouldFail {
            presenter?.didFail(with: MockSearchError.follow.rawValue)
        } else {
            presenter?.followDidSucceed()
        }
    }
    
    func fetchNextPage() {
        isFetchNextPageProcessing = true
        guard shouldContinue else { return }
        if shouldFail {
            presenter?.didFail(with: MockSearchError.fetchNextPage.rawValue)
        } else {
            presenter?.fetchNextPageDidSucceed()
        }
    }
    
    func numberOfItems() -> Int {
        30
    }
    
    func item(at index: Int) -> UserCellViewModel {
        .init(model: .init(id: 1, avatarUrl: nil, login: "James Moriarty"))
    }
    
    func hasNextPage() -> Bool {
        canFetchNext
    }

    func isLoading() -> Bool {
        loading
    }

    // MARK: - Worker
    func onSearchSuccess(items: [UserResponse], totalCount: Int) {
        self.searchItems = items
        self.totalCount = totalCount
    }
    
    func onFetchNextPageSuccess(items: [UserResponse]) {
        self.searchItems = items
    }
    
    func onFollowSuccess() {
        followSucceded = true
    }
    
    func onError(_ error: APIError) {
        errorMessage = error.title
    }
}
