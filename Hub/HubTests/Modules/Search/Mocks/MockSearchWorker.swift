//
//  MockSearchWorker.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockSearchWorker: SearchWorkerInterface {

    weak var interactor: SearchWorkerOutputProtocol?
    var shouldFail = false
    var loading = false

    func search(for query: String) {
        if shouldFail {
            interactor?.onError(.init(message: MockSearchError.search.rawValue))
        } else {
            interactor?.onSearchSuccess(items: [.init(id: 1, avatarUrl: nil, login: "Test")], totalCount: 3)
        }
    }
    
    func follow(username: String?) {
        if shouldFail {
            interactor?.onError(.init(message: MockSearchError.fetchNextPage.rawValue))
        } else {
            interactor?.onFollowSuccess()
        }
    }
    
    func fetchNextPage() {
        if shouldFail {
            interactor?.onError(.init(message: MockSearchError.follow.rawValue))
        } else {
            interactor?.onFetchNextPageSuccess(items: [.init(id: 1, avatarUrl: nil, login: "Test")])
        }
    }

    func isLoading() -> Bool {
        loading
    }
}
