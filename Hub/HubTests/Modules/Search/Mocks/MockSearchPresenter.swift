//
//  MockSearchPresenter.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockSearchPresenter: SearchInteractorOutputProtocol {

    var isSearchSucceded = false
    var isFetchNextPageSucceded = false
    var isFollowSucceded = false
    var errorMessage = ""

    func searchDidSucceed() {
        isSearchSucceded = true
    }

    func fetchNextPageDidSucceed() {
        isFetchNextPageSucceded = true
    }
    
    func followDidSucceed() {
        isFollowSucceded = true
    }
    
    func didFail(with message: String) {
        errorMessage = message
    }
}
