//
//  MockSearchError.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

enum MockSearchError: String {

    case search = "Error Search"
    case follow = "Error Follow"
    case fetchNextPage = "Error Fetch Next Page"
}
