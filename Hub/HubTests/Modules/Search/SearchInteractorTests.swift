//
//  SearchInteractorTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import XCTest
@testable import Hub

class SearchInteractorTests: XCTestCase {
    
    var sut: SearchInteractor!
    var worker: MockSearchWorker!
    var presenter: MockSearchPresenter!

    override func setUp() {
        sut = .init()
        worker = .init()
        presenter = .init()
        worker.interactor = sut
        sut.presenter = presenter
        sut.worker = worker
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        worker = nil
        presenter = nil
        super.tearDown()
    }

    // MARK: - Search
    func testFailsWhenNetworkErrorOccuredOnSearch() {
        worker.shouldFail = true
        sut.searchUsers(for: "a")
        presenter.errorMessage = MockSearchError.search.rawValue
    }

    func testPassesOnSearchSucceed() {
        sut.searchUsers(for: "a")
        XCTAssertTrue(presenter.isSearchSucceded)
    }

    func testClearsContentWhenQueryIsEmpty() {
        sut.searchUsers(for: "a")
        sut.searchUsers(for: "")
        XCTAssertEqual(sut.numberOfItems(), 0)
    }

    func testNumberOfItemsUpdatedOnSearch() {
        sut.searchUsers(for: "a")
        XCTAssertEqual(sut.numberOfItems(), 1)
    }

    func testItemAtIndexUpdatedOnSearch() {
        let user = UserCellViewModel(model: .init(id: 1, avatarUrl: nil, login: "Test"))
        sut.searchUsers(for: "a")
        let item = sut.item(at: 0)
        XCTAssertEqual(item.id, user.id)
        XCTAssertEqual(item.name, user.name)
        XCTAssertEqual(item.imageUrl, user.imageUrl)
    }

    // MARK: - Follow
    func testFailsWhenNetworkErrorOccuredOnFollow() {
        worker.shouldFail = true
        sut.followUser(username: "")
        presenter.errorMessage = MockSearchError.follow.rawValue
    }

    func testPassesOnFollowSucceed() {
        worker.follow(username: "")
        XCTAssertTrue(presenter.isFollowSucceded)
    }

    // MARK: - Fetch Next Page
    func testFailsWhenNetworkErrorOccuredOnFetchNextPage() {
        worker.shouldFail = true
        sut.fetchNextPage()
        presenter.errorMessage = MockSearchError.fetchNextPage.rawValue
    }

    func testPassesOnFetchNextPageSucceed() {
        worker.fetchNextPage()
        XCTAssertTrue(presenter.isFetchNextPageSucceded)
    }

    func testNumberOfItemsUpdatedOnFetchNextPage() {
        sut.searchUsers(for: "a")
        XCTAssertEqual(sut.numberOfItems(), 1)
    }

    func testItemAtIndexUpdatedOnFetchNextPage() {
        let user = UserCellViewModel(model: .init(id: 1, avatarUrl: nil, login: "Test"))
        sut.fetchNextPage()
        let item = sut.item(at: 0)
        XCTAssertEqual(item.id, user.id)
        XCTAssertEqual(item.name, user.name)
        XCTAssertEqual(item.imageUrl, user.imageUrl)
    }

    func testHasNextPage() {
        sut.searchUsers(for: "a")
        sut.fetchNextPage()
        XCTAssertTrue(sut.hasNextPage())
    }

    func testIsCurrentlyLoading() {
        worker.loading = true
        sut.fetchNextPage()
        XCTAssertTrue(sut.isLoading())
    }
}
