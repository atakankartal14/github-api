//
//  SearchPresenterTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import XCTest
@testable import Hub

class SearchPresenterTests: XCTestCase {

    var sut: SearchPresenter!
    var interactor: MockSearchInteractor!
    var view: MockSearchView!
    var router: MockSearchRouter!

    override func setUp() {
        interactor = .init()
        view = .init()
        router = .init()
        sut = .init(view: view, interactor: interactor, router: router)
        interactor.presenter = sut
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        interactor = nil
        view = nil
        router = nil
        super.tearDown()
    }

    func testViewSetsNavigationTitle() {
        sut.viewDidLoad()
        XCTAssertEqual(view.title, "HUB")
    }

    func testRouterShowsProfileOnTap() {
        sut.didTapShowProfile()
        XCTAssertTrue(router.didPush)
    }

    func testNumberOfItems() {
        XCTAssertEqual(sut.numberOfItems(), 30)
    }

    func testItemAtIndex() {
        let user = UserCellViewModel(model: UserResponse(id: 1, avatarUrl: nil, login: "James Moriarty"))
        let item = sut.item(at: 0)
        XCTAssertEqual(item.id, user.id)
        XCTAssertEqual(item.imageUrl, user.imageUrl)
        XCTAssertEqual(item.name, user.name)
    }

    // MARK: - Search
    func testInteractorStartsSearchUserProcess() {
        sut.searchTextDidChange("")
        XCTAssertTrue(interactor.isFetchUserProcessing)
    }

    func testViewShowsLoadingWhenSearchProcessStarted(){
        interactor.shouldContinue = false
        sut.searchTextDidChange("")
        XCTAssertTrue(view.isLoading)
    }

    func testViewHidesLoadingWhenErrorOccuredOnSearch() {
        interactor.shouldFail = true
        sut.searchTextDidChange("")
        XCTAssertFalse(view.isLoading)
    }

    func testRouterShowsAlertWhenErrorOccuredOnSearch() {
        interactor.shouldFail = true
        sut.searchTextDidChange("")
        XCTAssertEqual(router.alertMessage, MockSearchError.search.rawValue)
    }

    func testViewHidesLoadingWhenSearchProcessEnded() {
        sut.searchTextDidChange("")
        XCTAssertFalse(view.isLoading)
    }

    func testViewReloadsWhenSearchProcessEnded() {
        sut.searchTextDidChange("")
        XCTAssertTrue(view.didReload)
    }

    // MARK: - Follow
    func testInteractorStartsFollowProcess() {
        sut.didTapFollowUser(username: "")
        XCTAssertTrue(interactor.isFollowProcessing)
    }

    func testViewShowsLoadingWhenFollowProcessStarted(){
        interactor.shouldContinue = false
        sut.didTapFollowUser(username: "")
        XCTAssertTrue(view.isLoading)
    }

    func testViewHidesLoadingWhenErrorOccuredOnFollow() {
        interactor.shouldFail = true
        sut.didTapFollowUser(username: "")
        XCTAssertFalse(view.isLoading)
    }

    func testRouterShowsAlertWhenErrorOccuredOnFollow() {
        interactor.shouldFail = true
        sut.didTapFollowUser(username: "")
        XCTAssertEqual(router.alertMessage, MockSearchError.follow.rawValue)
    }

    func testViewHidesLoadingWhenFollowProcessEnded() {
        sut.didTapFollowUser(username: "")
        XCTAssertFalse(view.isLoading)
    }

    func testViewReloadsWhenFollowProcessEnded() {
        sut.didTapFollowUser(username: "")
        XCTAssertTrue(view.didReload)
    }

    // MARK: - Fetch Next Page
    func testViewDoesntShowLoadingForFetchNextPageIfThereIsNone() {
        interactor.canFetchNext = false
        sut.fetchNextPage()
        XCTAssertFalse(view.isLoading)
    }

    func testViewDoesntShowLoadingForFetchNextPageIfAlreadyLoading() {
        interactor.loading = true
        sut.fetchNextPage()
        XCTAssertFalse(view.isLoading)
    }
    
    func testInteractorStartsFetchNextPageUserProcess() {
        sut.fetchNextPage()
        XCTAssertTrue(interactor.isFetchNextPageProcessing)
    }

    func testViewShowsLoadingWhenFetchNextPageProcessStarted(){
        interactor.shouldContinue = false
        sut.fetchNextPage()
        XCTAssertTrue(view.isLoading)
    }

    func testViewHidesLoadingWhenErrorOccuredOnFetchNextPage() {
        interactor.shouldFail = true
        sut.fetchNextPage()
        XCTAssertFalse(view.isLoading)
    }

    func testRouterShowsAlertWhenErrorOccuredOnFetchNextPage() {
        interactor.shouldFail = true
        sut.fetchNextPage()
        XCTAssertEqual(router.alertMessage, MockSearchError.fetchNextPage.rawValue)
    }

    func testViewHidesLoadingWhenFetchNextPageProcessEnded() {
        sut.fetchNextPage()
        XCTAssertFalse(view.isLoading)
    }

    func testViewReloadsWhenFetchNextPageProcessEnded() {
        sut.fetchNextPage()
        XCTAssertTrue(view.didReload)
    }
}
