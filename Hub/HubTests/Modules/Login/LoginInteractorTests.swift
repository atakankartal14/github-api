//
//  LoginInteractorTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import XCTest
@testable import Hub

class LoginInteractorTests: XCTestCase {

    var sut: LoginInteractor!
    var presenter: MockLoginPresenter!
    var worker: MockLoginWorker!
    var invalidURL = URL(string: "https://google.com")!
    var validURL = URL(string: "hub://oauth/login?code=4305834086")!

    override func setUp() {
        sut = LoginInteractor()
        presenter = .init()
        worker = .init()
        sut.presenter = presenter
        sut.worker = worker
        worker.interactor = sut
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        worker = nil
        presenter = nil
        super.tearDown()
    }

    func testRetrievingOAuthRequest() {
        XCTAssertNotNil(sut.getOAuthRequest())
    }

    func testExtractAuthCodeFailsWhenFalseCallbackUrl() {
        sut.getAccessToken(with: invalidURL)
        XCTAssertFalse(presenter.authCodeExtracted)
    }

    func testExtractAuthCodeFailsWhenQueryHasNoParameterNamedCode() {
        sut.getAccessToken(with: invalidURL)
    }

    func testExtractAuthCodeSucceedWithValidUrl() {
        sut.getAccessToken(with: validURL)
        XCTAssertTrue(presenter.authCodeExtracted)
    }

    func testFailsWhenNetworkFails() {
        worker.shouldFail = true
        sut.getAccessToken(with: validURL)
        XCTAssertEqual(presenter.accessTokenErrorMessage, "Error")
    }

    func testAccessTokenRetrievedWhenNetworkSucceed() {
        sut.getAccessToken(with: validURL)
        XCTAssertTrue(presenter.hasAccessToken)
    }
}
