//
//  MockLoginInteractor.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockLoginInteractor: LoginInteractorInterface, LoginWorkerOutputProtocol {

    var returnOAuthRequest = true
    var isProcessing = false
    var shouldContinue = true
    var authCodeExtracted = true
    var shouldFail = false
    var workerSucceeeded = false
    var errorMessage = ""
    var presenter: LoginInteractorOutputProtocol?

    func getOAuthRequest() -> URLRequest? {
        if returnOAuthRequest {
            return URLRequest(url: URL(string: "https://www.google.com")!)
        } else {
            return nil
        }
    }

    func getAccessToken(with authUrl: URL) {
        isProcessing = true
        guard authCodeExtracted else { return }
        presenter?.extractionAuthCodeSucceed()
        guard shouldContinue else { return }
        if shouldFail {
            presenter?.acceessTokenFailed(with: "Error")
        } else {
            presenter?.acceessTokenRetrieved()
        }
    }

    // MARK: - Worker
    func onSuccess() {
        workerSucceeeded = true
    }
    
    func onError(_ error: APIError) {
        errorMessage = error.title
    }
}
