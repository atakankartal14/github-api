//
//  MockLoginView.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockLoginView: LoginViewInterface {

    var webViewLoaded = false
    var isLoading = false

    func loadWebView(_ request: URLRequest) {
        webViewLoaded = true
    }
    
    func showLoading() {
        isLoading = true
    }
    
    func hideLoading() {
        isLoading  = false
    }
}
