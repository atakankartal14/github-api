//
//  MockLoginWorker.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockLoginWorker: LoginWorkerInterface {

    var shouldFail = false
    var interactor: LoginWorkerOutputProtocol?
    
    func getAccessToken(with authCode: String) {
        if shouldFail {
            interactor?.onError(.init(message: "Error"))
        } else {
            interactor?.onSuccess()
        }
    }
}
