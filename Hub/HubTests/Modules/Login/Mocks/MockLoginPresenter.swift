//
//  MockLoginPresenter.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockLoginPresenter: LoginInteractorOutputProtocol {

    var hasAccessToken = false
    var authCodeExtracted = false
    var accessTokenErrorMessage = ""

    func extractionAuthCodeSucceed() {
        authCodeExtracted = true
    }

    func acceessTokenRetrieved() {
        hasAccessToken = true
    }
    
    func acceessTokenFailed(with message: String) {
        accessTokenErrorMessage = message
    }
}
