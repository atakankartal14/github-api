//
//  MockLoginRouter.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
@testable import Hub

class MockLoginRouter: LoginRouterInterface {

    var alertMessage = ""
    var isPushed = false

    func showAlert(message: String) {
        alertMessage = message
    }

    func showMainVC() {
        isPushed = true
    }
}
