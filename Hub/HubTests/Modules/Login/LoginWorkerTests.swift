//
//  LoginWorkerTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import XCTest
@testable import Hub

class LoginWorkerTests: XCTestCase {

    var sutSuccess: LoginWorker!
    var sutFail: LoginWorker!
    var interactor: MockLoginInteractor!

    override func setUp() {
        sutSuccess = .init(provider: MockAPI.oauthProviderSuccess)
        sutFail = .init(provider: MockAPI.oauthProviderFail)
        interactor = .init()
        sutSuccess.interactor = interactor
        sutFail.interactor = interactor
        super.setUp()
    }

    override func tearDown() {
        sutSuccess = nil
        sutFail = nil
        interactor = nil
        super.tearDown()
    }

    func testApiSucceeded() {
        sutSuccess.getAccessToken(with: "")
        XCTAssertTrue(interactor.workerSucceeeded)
    }

    func testAPIReturnsErrorOnFail() {
        sutFail.getAccessToken(with: "")
        XCTAssertEqual(interactor.errorMessage, "Error!")
    }
}
