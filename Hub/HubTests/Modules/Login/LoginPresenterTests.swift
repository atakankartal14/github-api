//
//  LoginPresenterTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 4.09.2021.
//

import XCTest
@testable import Hub

class LoginPresenterTests: XCTestCase {

    var sut: LoginPresenter!
    var view: MockLoginView!
    var interactor: MockLoginInteractor!
    var router: MockLoginRouter!
    var validUrl = URL(string: "https://google.com")!

    override func setUp() {
        view = .init()
        interactor = .init()
        router = .init()
        sut = .init(view: view, interactor: interactor, router: router)
        interactor.presenter = sut
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        interactor = nil
        router = nil
        view = nil
        super.tearDown()
    }

    func testViewDoesntLoadWebViewWhenNoValidOAuthUrl() {
        interactor.returnOAuthRequest = false
        sut.viewDidLoad()
        XCTAssertFalse(view.webViewLoaded)
    }

    func testViewLoadsWebViewOnViewDidLoad() {
        sut.viewDidLoad()
        XCTAssertEqual(view.webViewLoaded, true)
    }

    func testInteractorStartsAccessTokenProcess() {
        sut.getAccessToken(with: validUrl)
        XCTAssertTrue(interactor.isProcessing)
    }

    func testViewDoesntShowLoadingWhenAuthCodeNotExtracted() {
        interactor.authCodeExtracted = false
        XCTAssertFalse(view.isLoading)
    }

    func testViewShowsLoadingWhenAccessTokenProcessBegins() {
        interactor.shouldContinue = false
        sut.getAccessToken(with: validUrl)
        XCTAssertTrue(view.isLoading)
    }

    func testViewHidesLoadingWhenAccessTokenProcessEnds() {
        sut.getAccessToken(with: validUrl)
        XCTAssertFalse(view.isLoading)
    }

    func testRouterShowsAlertWhenAPIErrorOccured() {
        interactor.shouldFail = true
        sut.getAccessToken(with: validUrl)
        XCTAssertEqual(router.alertMessage, "Error")
    }

    func testRouterShowsMainScreenWhenAccessTokenRetrieved() {
        sut.getAccessToken(with: validUrl)
        XCTAssertTrue(router.isPushed)
    }
}
