//
//  ProfileInteractorTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import XCTest
@testable import Hub

class ProfileInteractorTests: XCTestCase {

    var sut: ProfileInteractor!
    var worker: MockProfileWorker!
    var presenter: MockProfilePresenter!

    override func setUp() {
        sut = .init()
        worker = .init()
        presenter = .init()
        worker.interactor = sut
        sut.presenter = presenter
        sut.worker = worker
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        worker = nil
        presenter = nil
        super.tearDown()
    }

    func testFailsWhenNetworkErrorOccured() {
        worker.shouldFail = true
        sut.fetch()
        XCTAssertEqual(presenter.errorMessage, "Error")
    }

    func testPassesWhenNetworkSucceeded() {
        sut.fetch()
        XCTAssertTrue(presenter.didSucceed)
    }

    func testNumberOfItemsUpdatedWhenNetworkSucceded() {
        sut.fetch()
        XCTAssertEqual(sut.numberOfItems(), 6)
    }

    func testItemAtIndexUpdatedWhenNetworkSucceded() {
        sut.fetch()
        XCTAssertEqual(sut.item(at: 0), ProfileCellType.header(vm: .init(imageUrl: URL(string: "www.google.com"), name: "name")))
        XCTAssertEqual(sut.item(at: 1), ProfileCellType.info(vm: .init(title: "E-mail", description: "email")))
        XCTAssertEqual(sut.item(at: 2), ProfileCellType.info(vm: .init(title: "Public Repos", description: "1")))
        XCTAssertEqual(sut.item(at: 3), ProfileCellType.info(vm: .init(title: "Followers", description: "1")))
        XCTAssertEqual(sut.item(at: 4), ProfileCellType.info(vm: .init(title: "Followings", description: "1")))
        XCTAssertEqual(sut.item(at: 5), ProfileCellType.info(vm: .init(title: "Location", description: "location")))
    }

    func testItemAtIndexDescriptionDefaultValuesSetWhenNoDescription() {
        worker.hasDescriptions = false
        sut.fetch()
        XCTAssertEqual(sut.item(at: 0), ProfileCellType.header(vm: .init(imageUrl: nil, name: "-")))
        XCTAssertEqual(sut.item(at: 1), ProfileCellType.info(vm: .init(title: "E-mail", description: "-")))
        XCTAssertEqual(sut.item(at: 2), ProfileCellType.info(vm: .init(title: "Public Repos", description: "0")))
        XCTAssertEqual(sut.item(at: 3), ProfileCellType.info(vm: .init(title: "Followers", description: "0")))
        XCTAssertEqual(sut.item(at: 4), ProfileCellType.info(vm: .init(title: "Followings", description: "0")))
        XCTAssertEqual(sut.item(at: 5), ProfileCellType.info(vm: .init(title: "Location", description: "-")))
    }
}
