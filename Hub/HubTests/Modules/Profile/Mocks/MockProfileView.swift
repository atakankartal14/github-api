//
//  MockProfileView.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation
@testable import Hub

class MockProfileView: ProfileViewInterface {

    var title = ""
    var didReload = false
    var isLoading = false

    func setTitle(_ text: String) {
        title = text
    }
    
    func reload() {
        didReload = true
    }
    
    func showLoading() {
        isLoading = true
    }
    
    func hideLoading() {
        isLoading = false
    }
    
}
