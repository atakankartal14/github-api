//
//  MockProfileInteractor.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation
@testable import Hub

class MockProfileInteractor: ProfileInteractorInterface, ProfileWorkerOutputProtocol {

    var presenter: ProfileInteractorOutputProtocol!
    var isProcessing = false
    var shouldContinue = true
    var shouldFail = false
    var searchSucceeded = false
    var errorMessage = ""
    
    func fetch() {
        isProcessing = true
        guard shouldContinue else { return }
        if shouldFail {
            presenter.didFail(with: "Error")
        } else {
            presenter.fetchDidSucceed()
        }
    }
    
    func numberOfItems() -> Int {
        3
    }
    
    func item(at index: Int) -> ProfileCellType {
        .info(vm: .init(title: "Title", description: "Desc"))
    }

    // Worker
    func onSearchSuccess(_ response: ProfileResponse) {
        searchSucceeded = true
    }
    
    func onError(_ error: APIError) {
        errorMessage = error.title
    }
}
