//
//  MockProfileRouter.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation
@testable import Hub

class MockProfileRouter: ProfileRouterInterface {

    var alertMessage = ""
    
    func showAlert(message: String) {
        alertMessage = message
    }
}
