//
//  MockProfilePresenter.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation
@testable import Hub

class MockProfilePresenter: ProfileInteractorOutputProtocol {

    var didSucceed = false
    var errorMessage = ""
    
    func fetchDidSucceed() {
        didSucceed = true
    }
    
    func didFail(with message: String) {
        errorMessage = message
    }
}
