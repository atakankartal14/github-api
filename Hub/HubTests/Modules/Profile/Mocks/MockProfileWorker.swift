//
//  MockProfileWorker.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation
@testable import Hub

class MockProfileWorker: ProfileWorkerInterface {

    var interactor: ProfileWorkerOutputProtocol!
    var shouldFail = false
    var hasDescriptions = true

    func fetch() {
        if shouldFail {
            interactor.onError(.init(message: "Error"))
        } else {
            if hasDescriptions {
                interactor.onSearchSuccess(.init(id: nil,
                                                 name: "name",
                                                 avatarUrl: URL(string: "www.google.com"),
                                                 email: "email",
                                                 publicRepos: 1,
                                                 followers: 1,
                                                 following: 1,
                                                 location: "location"))
            } else {
                interactor.onSearchSuccess(.init(id: nil,
                                                 name: nil,
                                                 avatarUrl: nil,
                                                 email: nil,
                                                 publicRepos: nil,
                                                 followers: nil,
                                                 following: nil,
                                                 location: nil))
            }
        }
    }
}
