//
//  ProfileWorkerTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import XCTest
@testable import Hub

class ProfileWorkerTests: XCTestCase {

    var sutSuccess: ProfileWorker!
    var sutFail: ProfileWorker!
    var interactor: MockProfileInteractor!

    override func setUp() {
        sutSuccess = .init(provider: MockAPI.userProviderSuccess)
        sutFail = .init(provider: MockAPI.userProviderFail)
        interactor = .init()
        sutSuccess.interactor = interactor
        sutFail.interactor = interactor
        super.setUp()
    }

    override func tearDown() {
        sutSuccess = nil
        sutFail = nil
        interactor = nil
        super.tearDown()
    }

    func testApiSucceeded() {
        sutSuccess.fetch()
        XCTAssertTrue(interactor.searchSucceeded)
    }

    func testAPIReturnsErrorOnFail() {
        sutFail.fetch()
        XCTAssertEqual(interactor.errorMessage, "Error!")
    }
}
