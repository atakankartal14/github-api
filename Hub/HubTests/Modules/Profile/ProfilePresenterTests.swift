//
//  ProfilePresenterTests.swift
//  HubTests
//
//  Created by Atakan Kartal on 5.09.2021.
//

import XCTest
@testable import Hub

class ProfilePresenterTests: XCTestCase {

    var sut: ProfilePresenter!
    var interactor: MockProfileInteractor!
    var view: MockProfileView!
    var router: MockProfileRouter!

    override func setUp() {
        interactor = .init()
        view = .init()
        router = .init()
        sut = .init(view: view, interactor: interactor, router: router)
        interactor.presenter = sut
        super.setUp()
    }

    override func tearDown() {
        sut = nil
        interactor = nil
        view = nil
        router = nil
        super.tearDown()
    }

    func testViewSetsTitleWhenViewLoaded() {
        sut.viewDidLoad()
        XCTAssertEqual(view.title, "Profile")
    }

    func testInteractorStartsFetchProcessWhenViewLoaded() {
        interactor.shouldContinue = false
        sut.viewDidLoad()
        XCTAssertTrue(interactor.isProcessing)
    }

    func testViewShowsLoadingWhenFetchProcessStarted() {
        interactor.shouldContinue = false
        sut.viewDidLoad()
        XCTAssertTrue(view.isLoading)
    }

    func testRouterShowsAlertWhenNetworkFailed() {
        interactor.shouldFail = true
        sut.viewDidLoad()
        XCTAssertEqual(router.alertMessage, "Error")
    }

    func testViewHidesLoadingWhenFetchProcessSucceeded() {
        sut.viewDidLoad()
        XCTAssertFalse(view.isLoading)
    }

    func testViewReloadsWhenFetchProcessSucceeded() {
        sut.viewDidLoad()
        XCTAssertTrue(view.didReload)
    }

    func testNumberOfItemsUpdatedWhenFetchProcessSucceeded() {
        sut.viewDidLoad()
        XCTAssertEqual(sut.numberOfItems(), 3)
    }

    func testItemAtIndexUpdatedWhenFetchProcessSucceeded() {
        sut.viewDidLoad()
        let cell = ProfileCellType.info(vm: .init(title: "Title", description: "Desc"))
        let item = sut.item(at: 0)
        XCTAssertEqual(cell, item)
    }
}
