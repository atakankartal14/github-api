//
//  UIApplication+Ext.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import UIKit

extension UIApplication {

    func show(_ vc: UIViewController) {
        let window = getCurrentWindow()
        window.rootViewController = vc
        window.makeKeyAndVisible()
        vc.modalTransitionStyle = .crossDissolve
        UIView.transition(with: window, duration: 0.2, options: .transitionCrossDissolve, animations: nil, completion: nil)
    }

    func getCurrentWindow() -> UIWindow {
        guard
            let sharedWindow = UIApplication.shared.delegate?.window,
            let window = sharedWindow
        else {
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.makeKeyAndVisible()
            return window
        }
        return window
    }
}
