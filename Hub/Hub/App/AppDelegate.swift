//
//  AppDelegate.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.makeKeyAndVisible()
        let hasToken = !(TokenManager.shared.accessToken ?? "").isEmpty
        window?.rootViewController = hasToken ? SearchRouter.create() : LoginRouter.create()
        return true
    }

}

