//
//  EnvironmentType.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Foundation

protocol EnvironmentType {

    /// Base API URL
    static var baseUrl: URL { get }

    /// OAuth URL
    static var oauthUrl: URL { get }

    /// OAurh Clien tID
    static var oauthClientId: String { get }

    /// OAuth Client Secret
    static var oauthClientSecret: String { get }
}
