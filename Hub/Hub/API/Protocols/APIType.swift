//
//  APIType.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Foundation

protocol APIType {

    associatedtype E: EnvironmentType
}
