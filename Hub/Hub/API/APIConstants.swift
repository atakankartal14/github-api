//
//  APIConstants.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Foundation

struct APIConstants {

    static let oauthCallbackURL = "hub://oauth/login"

    static let oauthScope = "read:user,user:email,user:follow"

    static let oauthGrantType = "authorization_code"

    static var headers: [String : String]? {
        [
            "Content-Type": "application/json",
            "Accept": "application/vnd.github.v3+json"
        ]
    }
}
