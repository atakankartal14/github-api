//
//  Moya+Request.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Moya

/// Methods for custom API response handling
extension MoyaProvider {

    /// Generic Result Type
    public typealias APIResult<T: Codable> = (Result<T, APIError>) -> Void

    /// Custom API request method
    /// - Parameters:
    ///   - target: Target Type that the request will be sent for
    ///   - responseType: Expected response type of the request
    ///   - completion: Completion handler that returns APIResult 'MLBaseResponse<T>' or 'MLAPIError'
    /// - Returns: Request that is sent (in order to cancel at any point)
    @discardableResult
    public func request<T: Codable>(_ target: Target,
                                    responseType: T.Type,
                                    completion: @escaping APIResult<T>) -> Cancellable {
        sendRequest(target, responseType: responseType, completion: completion)
    }

    /// Custom Decoder
    private var jsonDecoder: JSONDecoder {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return jsonDecoder
    }

    /// Sends request for given target
    private func sendRequest<T: Codable>(_ target: Target,
                                         responseType: T.Type,
                                         completion: @escaping APIResult<T>) -> Cancellable {
        request(target) { (result) in
            switch result {
            case .failure(let error):
                let error = APIError(title: "Error!", message: error.localizedDescription, code: error.response?.statusCode)
                completion(.failure(error))
            case .success(let response):
                guard self.validate(response, completion: completion) else { return }
                do {
                    let model = try response.map(T.self,
                                                 using: self.jsonDecoder,
                                                 failsOnEmptyData: false)
                    completion(.success(model))
                } catch MoyaError.statusCode(let response) {
                    let error = APIError(title: "Error!", message: "Unexpected Status Code", code: response.statusCode, response: response)
                    completion(.failure(error))
                } catch let MoyaError.objectMapping(error, response) {
                    debugPrint("DECODING ERROR: ", error)
                    let error = APIError(title: "Decoding Error", message: "Failed to decode object", response: response)
                    completion(.failure(error))
                } catch {
                    let error = APIError(title: "Error!", message: error.localizedDescription, response: response)
                    completion(.failure(error))
                }
            }
        }
    }

    /// Validates the response for appropriate status code range
    private func validate<T: Codable>(_ response: Response, completion: @escaping APIResult<T>) -> Bool {
        do {
            _ = try response.filterSuccessfulStatusCodes()
            return true
        } catch {
            if let errorResponse = try? jsonDecoder.decode(ErrorResponse.self, from: response.data) {
                let error = APIError(title: "Error!",
                                     message: errorResponse.message ?? "Unknown error",
                                     code: nil,
                                     payload: nil,
                                     response: response)
                completion(.failure(error))
                return false
            }
            let error = APIError(title: "Error!",
                                 message: error.localizedDescription,
                                 code: nil,
                                 payload: nil,
                                 response: response)
            completion(.failure(error))
            return false
        }
    }
}
