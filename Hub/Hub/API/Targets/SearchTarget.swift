//
//  SearchProvider.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Moya

enum SearchTarget<E: EnvironmentType> {

    case search(_ req: UsersRequest)
}

extension SearchTarget: TargetType {

    var baseURL: URL {
        E.baseUrl.appendingPathComponent("search")
    }

    var path: String {
        switch self {
        case .search:
            return "users"
        }
    }

    var method: Moya.Method {
        .get
    }

    var task: Task {
        switch self {
        case .search(let req):
            return req.urlEncodedQueryString
        }
    }

    var headers: [String : String]? {
        APIConstants.headers
    }

    var sampleData: Data {
        """
        {
        "totalCount": 5,
        "items": [
        {
            "id": 1,
            "avatarUrl": null,
            "login": "name"
        }
        ]
        }
        """.data(using: .utf8) ?? .init()
    }
}

extension SearchTarget: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType? {
        .bearer
    }
}
