//
//  UserProvider.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Moya

enum UserTarget<E: EnvironmentType> {

    case profile
    case follow(_ req: FollowUserRequest)
}

// MARK: - Target Type
extension UserTarget: TargetType {

    var baseURL: URL {
        E.baseUrl.appendingPathComponent("user")
    }
    
    var path: String {
        switch self {
        case .profile:
            return ""
        case .follow(let req):
            return "following/\(req.username ?? "")"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .profile:
            return .get
        case .follow:
            return .put
        }
    }
    
    var task: Task {
        switch self {
        case .profile:
            return .requestPlain
        case .follow:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        APIConstants.headers
    }
    
    var sampleData: Data {
        """
        {"id": 1}
        """.data(using: .utf8) ?? .init()
    }
}

// MARK: - AccessTokenAuthorizable
extension UserTarget: AccessTokenAuthorizable {
    var authorizationType: AuthorizationType? {
        .bearer
    }
}
