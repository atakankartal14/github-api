//
//  OAuthProvider.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Moya

enum OAuthTarget<E: EnvironmentType> {

    case authorize(_ req: AuthorizeRequest)
    case accessToken(_ req: AccessTokenRequest)
}

extension OAuthTarget: TargetType {

    var baseURL: URL {
        E.oauthUrl
    }
    
    var path: String {
        switch self {
        case .authorize:
            return "authorize"
        case .accessToken:
            return "access_token"
        }
    }

    var method: Moya.Method {
        switch self {
        case .authorize:
            return .get
        case .accessToken:
            return .post
        }
    }

    var task: Task {
        switch self {
        case .authorize(let req):
            return req.urlEncodedQueryString
        case .accessToken(let req):
            return req.jsonEncoded
        }
    }

    var headers: [String : String]? {
        APIConstants.headers
    }

    var sampleData: Data {
        """
        {"accessToken": "TOKEN"}
        """.data(using: .utf8) ?? .init()
    }

}
