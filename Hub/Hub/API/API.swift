//
//  API.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Moya

class API: APIType {

    #if DEBUG
    typealias E = TestEnvironment
    #else
    typealias E = ProdEnvironment
    #endif

    /// OAuth Provider
    static var oauthProvider = MoyaProvider<OAuthTarget<E>>(plugins: TargetPlugins.plugins)

    /// Search Provider
    static var searchProvider = MoyaProvider<SearchTarget<E>>(plugins: TargetPlugins.plugins)

    /// User Provider
    static var userProvider = MoyaProvider<UserTarget<E>>(plugins: TargetPlugins.plugins)

}
