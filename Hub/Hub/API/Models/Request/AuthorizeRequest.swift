//
//  AuthorizeRequest.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Foundation

struct AuthorizeRequest: Codable {

    let clientId: String
    let scope: String
    let redirectURL: String
    let state: String

    enum CodingKeys: String, CodingKey {
        case clientId = "client_id"
        case scope
        case redirectURL = "redirect_uri"
        case state
    }

}
