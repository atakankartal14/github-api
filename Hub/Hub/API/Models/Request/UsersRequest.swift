//
//  UsersRequest.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

struct UsersRequest: Codable {

    let query: String?
    let page: Int?

    enum CodingKeys: String, CodingKey {
        case query = "q"
        case page
    }
}
