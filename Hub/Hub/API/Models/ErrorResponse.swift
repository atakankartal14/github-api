//
//  ErrorResponse.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

struct ErrorResponse: Codable {

    let message: String?
}
