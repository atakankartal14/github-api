//
//  AccessTokenResponse.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

struct AccessTokenResponse: Codable {

    let accessToken: String?

}
