//
//  ProfileResponse.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

struct ProfileResponse: Codable {

    let id: Int?
    let name: String?
    let avatarUrl: URL?
    let email: String?
    let publicRepos: Int?
    let followers: Int?
    let following: Int?
    let location: String?
}
