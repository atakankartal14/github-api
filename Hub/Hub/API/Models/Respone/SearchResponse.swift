//
//  SearchResponse.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

struct SearchResponse: Codable {

    let totalCount: Int?
    let items: [UserResponse]?
}

struct UserResponse: Codable {

    let id: Int?
    let avatarUrl: URL?
    let login: String?
}
