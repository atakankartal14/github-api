//
//  Encodable+Ext.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Moya

extension Encodable {

    var jsonEncoded: Task {
        var jsonEncoder: JSONEncoder {
            let jsonEncoder = JSONEncoder()
            jsonEncoder.dateEncodingStrategy = .iso8601
            return jsonEncoder
        }
        return Task.requestCustomJSONEncodable(self, encoder: jsonEncoder)
    }

    var urlEncodedQueryString: Task {
        dictionary.urlEncodeQueryString
    }

    var dictionary: [String: Any] {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        guard let data = try? encoder.encode(self) else {
            return [:]
        }
        let dict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
        return dict ?? [:]
    }

}
