//
//  Dictionary+Ext.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Moya

extension Dictionary {

    /// Converts codable request type to Moya.Task
    public var urlEncodeQueryString: Task {
        guard let parameters = self.compactMapValues({ $0 }) as? [String: Any] else { return .requestPlain }
        return Task.requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
    }

}
