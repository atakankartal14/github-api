//
//  TestEnvironment.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Foundation

struct TestEnvironment: EnvironmentType {

    static var baseUrl: URL {
        return URL(string: "https://api.github.com")!
    }

    static var oauthUrl: URL {
        return URL(string: "https://github.com/login/oauth")!
    }

    static var oauthClientId: String {
        "820e1c26066cf4a236d8"
    }

    static var oauthClientSecret: String {
        "7b3f0c5fac9f5cba6c9d31e7373f537b74b81139"
    }

}
