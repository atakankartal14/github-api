//
//  TokenManager.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

class TokenManager {

    static var shared = TokenManager()

    private init() {}

    @UserDefaultsWrapper(key: "ud_AccessToken", defaultValue: nil)
    var accessToken: String?
}
