//
//  BearerTokenPlugin.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Moya

struct BearerTokenPlugin {

    static var closure: AccessTokenPlugin.TokenClosure = { authType in
        return TokenManager.shared.accessToken ?? ""
    }
}
