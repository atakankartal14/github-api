//
//  CustomPlugin.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Moya
import UIKit

class CustomPlugin: PluginType {

    func willSend(_ request: RequestType, target: TargetType) {
        debugPrint("Request will send for: ", target.path)
    }

    func didReceive(_ result: Result<Response, MoyaError>, target: TargetType) {
        debugPrint("Response did receive for: ", target.path)
        //NavigationManager.shared.showLoading(false)
        switch result {
        case .failure(let error):
            if error.response?.statusCode == 401 {
                UIApplication.shared.show(LoginRouter.create())
            }
        case .success(let response):
            if response.statusCode == 401 {
                UIApplication.shared.show(LoginRouter.create())
            }
        }
    }
}
