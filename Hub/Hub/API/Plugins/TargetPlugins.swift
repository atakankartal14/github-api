//
//  TargetPlugins.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import Moya

struct TargetPlugins {

    static var plugins: [PluginType] = [
        AccessTokenPlugin(tokenClosure: BearerTokenPlugin.closure),
        CustomPlugin(),
        NetworkLoggerPlugin(configuration: NetworkLoggerPlugin.Configuration(logOptions: [.formatRequestAscURL,
                                                                                          .verbose]))
    ]
}
