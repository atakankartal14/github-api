//
//  SearchWorker.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Moya

class SearchWorker {

    typealias ProviderSearch = MoyaProvider<SearchTarget<API.E>>
    typealias ProviderUser = MoyaProvider<UserTarget<API.E>>

    weak var interactor: SearchWorkerOutputProtocol!
    private let searchProvider: ProviderSearch
    private let userProvider: ProviderUser
    private var query: String?
    private var page = 1
    private var loading = false

    init(searchProvider: ProviderSearch = API.searchProvider, userProvider: ProviderUser = API.userProvider) {
        self.searchProvider = searchProvider
        self.userProvider = userProvider
    }

    private func fetch(reload: Bool = false) {
        loading = true
        if reload {
            page = 1
        }
        searchProvider.request(.search(.init(query: query, page: page)), responseType: SearchResponse.self) { [weak self] result in
            self?.loading = false
            switch result {
            case .failure(let error):
                self?.interactor.onError(error)
            case .success(let response):
                let items = response.items ?? []
                if reload {
                    self?.interactor.onSearchSuccess(items: items, totalCount: response.totalCount ?? 0)
                } else {
                    self?.interactor.onFetchNextPageSuccess(items: items)
                }
            }
        }
    }
}

// MARK: - SearchWorker Interface
extension SearchWorker: SearchWorkerInterface {
    func search(for query: String) {
        self.query = query
        fetch(reload: true)
    }
    
    func follow(username: String?) {
        userProvider.request(.follow(.init(username: username)), responseType: EmptyResponse.self) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.interactor.onError(error)
            case .success:
                self?.interactor.onFollowSuccess()
            }
        }
    }

    func fetchNextPage() {
        page += 1
        fetch(reload: false)
    }

    func isLoading() -> Bool {
        loading
    }
}
