//
//  SearchRouter.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import UIKit

class SearchRouter {

    var navigationController: UINavigationController!

    static func create() -> UINavigationController {
        let view = SearchViewController()
        let interactor = SearchInteractor()
        let router = SearchRouter()
        let worker = SearchWorker()
        let presenter = SearchPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.worker = worker
        worker.interactor = interactor
        let navigation = UINavigationController(rootViewController: view)
        router.navigationController = navigation
        return navigation
    }
}

// MARK: - SearchRouter Interface
extension SearchRouter: SearchRouterInterface {
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        navigationController.present(alert, animated: true, completion: nil)
    }
    
    func showProfile() {
        navigationController.pushViewController(ProfileRouter.create(from: navigationController), animated: true)
    }
}
