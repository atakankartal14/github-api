//
//  SearchPresenter.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

class SearchPresenter: SearchPresenterInterface {

    weak var view: SearchViewInterface?
    var interactor: SearchInteractorInterface
    var router: SearchRouterInterface

    init(view: SearchViewInterface, interactor: SearchInteractorInterface, router: SearchRouterInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - SearchView Output
extension SearchPresenter: SearchViewOutputProtocol {
    func viewDidLoad() {
        view?.setTitle("HUB")
    }
    
    func searchTextDidChange(_ text: String) {
        view?.showLoading()
        interactor.searchUsers(for: text)
    }
    
    func didTapFollowUser(username: String?) {
        view?.showLoading()
        interactor.followUser(username: username)
    }

    func fetchNextPage() {
        if interactor.hasNextPage() && !interactor.isLoading() {
            view?.showLoading()
            interactor.fetchNextPage()
        }
    }

    func didTapShowProfile() {
        router.showProfile()
    }

    func numberOfItems() -> Int {
        interactor.numberOfItems()
    }
    
    func item(at index: Int) -> UserCellViewModel {
        interactor.item(at: index)
    }
}

// MARK: - SearchInteractor Output
extension SearchPresenter: SearchInteractorOutputProtocol {
    func searchDidSucceed() {
        view?.hideLoading()
        view?.reload()
    }

    func fetchNextPageDidSucceed() {
        view?.hideLoading()
        view?.reload()
    }
    
    func followDidSucceed() {
        view?.hideLoading()
        view?.reload()
    }
    
    func didFail(with message: String) {
        view?.hideLoading()
        router.showAlert(message: message)
    }
}
