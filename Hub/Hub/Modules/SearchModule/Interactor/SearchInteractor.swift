//
//  SearchInteractor.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

class SearchInteractor {

    weak var presenter: SearchInteractorOutputProtocol!
    var worker: SearchWorkerInterface!

    private var cellVMs = [UserCellViewModel]()
    private var totalCount = 0
}

// MARK: - SearchInteractor Interface
extension SearchInteractor: SearchInteractorInterface {
    func searchUsers(for query: String) {
        if !query.isEmpty {
            worker?.search(for: query)
        } else {
            cellVMs.removeAll()
            totalCount = 0
            presenter.searchDidSucceed()
        }
    }
    
    func followUser(username: String?) {
        worker.follow(username: username)
    }
    
    func fetchNextPage() {
        worker.fetchNextPage()
    }
    
    func numberOfItems() -> Int {
        cellVMs.count
    }
    
    func item(at index: Int) -> UserCellViewModel {
        cellVMs[index]
    }

    func hasNextPage() -> Bool {
        totalCount > cellVMs.count
    }

    func isLoading() -> Bool {
        worker.isLoading()
    }
}

// MARK: - SearchWorker Output
extension SearchInteractor: SearchWorkerOutputProtocol {
    func onSearchSuccess(items: [UserResponse], totalCount: Int) {
        self.totalCount = totalCount
        cellVMs.removeAll()
        cellVMs = items.map({ .init(model: $0) })
        presenter?.searchDidSucceed()
    }
    
    func onFetchNextPageSuccess(items: [UserResponse]) {
        cellVMs.append(contentsOf: items.map({ .init(model: $0) }))
        presenter?.fetchNextPageDidSucceed()
    }
    
    func onFollowSuccess() {
        presenter?.followDidSucceed()
        presenter?.followDidSucceed()
    }
    
    func onError(_ error: APIError) {
        presenter?.didFail(with: error.message)
    }
}
