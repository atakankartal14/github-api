//
//  UserCellViewModel.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

class UserCellViewModel {

    let id: Int?
    let name: String
    let imageUrl: URL?

    init(model: UserResponse) {
        self.id = model.id
        self.name = model.login ?? ""
        self.imageUrl = model.avatarUrl
    }
}
