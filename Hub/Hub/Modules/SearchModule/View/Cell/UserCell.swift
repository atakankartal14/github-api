//
//  UserCell.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import UIKit
import Reusable
import Kingfisher

protocol UserCellDelegate: AnyObject {
    func didTapFollowUser(username: String?)
}

class UserCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var followButton: UIButton!

    private var username: String?
    private weak var delegate: UserCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()

        avatarImageView.layer.cornerRadius = 25
        followButton.layer.cornerRadius = 12
    }

    public func configure(model: UserCellViewModel, delegate: UserCellDelegate?) {
        self.username = model.name
        self.delegate = delegate
        avatarImageView.kf.setImage(with: model.imageUrl)
        nameLabel.text = model.name
    }
    
    @IBAction private func didTapFollow(_ sender: UIButton) {
        delegate?.didTapFollowUser(username: username)
    }
}
