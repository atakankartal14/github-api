//
//  SearchViewController.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import UIKit
import SVProgressHUD

class SearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    var presenter: SearchViewOutputProtocol!
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.viewDidLoad()
        tableView.register(cellType: UserCell.self)
        navigationItem.rightBarButtonItem = .init(image: .init(systemName: "person.circle.fill"),
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(didTapProfile))
    }

    @objc func search(_ text: String) {
        presenter.searchTextDidChange(text)
    }

    @objc func didTapProfile() {
        presenter.didTapShowProfile()
    }
}

// MARK: - SearchView Interface
extension SearchViewController: SearchViewInterface {
    func setTitle(_ text: String) {
        title = text
    }
    
    func reload() {
        tableView.reloadData()
    }
    
    func showLoading() {
        SVProgressHUD.show()
    }
    
    func hideLoading() {
        SVProgressHUD.dismiss()
    }
}

// MARK: - TableView Delegate & DataSource
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: UserCell.self)
        cell.configure(model: presenter.item(at: indexPath.row), delegate: self)
        return cell
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height {
            presenter.fetchNextPage()
        }
    }
}

// MARK: - UISearchBar Delegate
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false, block: { _ in
            self.search(searchText)
        })
    }
}

// MARK: - UserCell Delegate
extension SearchViewController: UserCellDelegate {
    func didTapFollowUser(username: String?) {
        guard let username = username else { return }
        presenter.didTapFollowUser(username: username)
    }
}
