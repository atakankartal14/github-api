//
//  SearchContracts.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

protocol SearchViewInterface: AnyObject {
    func setTitle(_ text: String)
    func reload()
    func showLoading()
    func hideLoading()
}

protocol SearchViewOutputProtocol {
    func viewDidLoad()
    func searchTextDidChange(_ text: String)
    func didTapFollowUser(username: String?)
    func didTapShowProfile()
    func fetchNextPage()
    func numberOfItems() -> Int
    func item(at index: Int) -> UserCellViewModel
}

protocol SearchInteractorInterface {
    func searchUsers(for query: String)
    func followUser(username: String?)
    func fetchNextPage()
    func numberOfItems() -> Int
    func item(at index: Int) -> UserCellViewModel
    func hasNextPage() -> Bool
    func isLoading() -> Bool
}

protocol SearchInteractorOutputProtocol: AnyObject {
    func searchDidSucceed()
    func fetchNextPageDidSucceed()
    func followDidSucceed()
    func didFail(with message: String)
}

protocol SearchWorkerInterface {
    func search(for query: String)
    func follow(username: String?)
    func fetchNextPage()
    func isLoading() -> Bool
}

protocol SearchWorkerOutputProtocol: AnyObject {
    func onSearchSuccess(items: [UserResponse], totalCount: Int)
    func onFetchNextPageSuccess(items: [UserResponse])
    func onFollowSuccess()
    func onError(_ error: APIError)
}

protocol SearchRouterInterface {
    func showAlert(message: String)
    func showProfile()
}

protocol SearchPresenterInterface {
    var view: SearchViewInterface? { get set }
    var interactor: SearchInteractorInterface { get set }
    var router: SearchRouterInterface { get set }
}
