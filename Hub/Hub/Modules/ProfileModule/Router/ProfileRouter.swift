//
//  ProfileRouter.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import UIKit

class ProfileRouter {

    var navigationController: UINavigationController!

    static func create(from navigation: UINavigationController) -> ProfileViewController {
        let view = ProfileViewController()
        let interactor = ProfileInteractor()
        let router = ProfileRouter()
        let worker = ProfileWorker()
        let presenter = ProfilePresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.worker = worker
        worker.interactor = interactor
        router.navigationController = navigation
        return view
    }
}

// MARK: - ProfileRouter Interface
extension ProfileRouter: ProfileRouterInterface {
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        navigationController.present(alert, animated: true, completion: nil)
    }
}
