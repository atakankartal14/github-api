//
//  ProfileWorker.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Moya

class ProfileWorker {

    typealias Provider = MoyaProvider<UserTarget<API.E>>

    weak var interactor: ProfileWorkerOutputProtocol!
    private let provider: Provider

    init(provider: Provider = API.userProvider) {
        self.provider = provider
    }
}

// MARK: - ProfileWorker Interface
extension ProfileWorker: ProfileWorkerInterface {
    func fetch() {
        provider.request(.profile, responseType: ProfileResponse.self) { [weak self] result in
            switch result {
            case .failure(let error):
                self?.interactor.onError(error)
            case .success(let response):
                self?.interactor.onSearchSuccess(response)
            }
        }
    }
}
