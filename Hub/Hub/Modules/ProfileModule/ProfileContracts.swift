//
//  ProfileContracts.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

protocol ProfileViewInterface: AnyObject {
    func setTitle(_ text: String)
    func reload()
    func showLoading()
    func hideLoading()
}

protocol ProfileViewOutputProtocol {
    func viewDidLoad()
    func numberOfItems() -> Int
    func item(at index: Int) -> ProfileCellType
}

protocol ProfileInteractorInterface {
    func fetch()
    func numberOfItems() -> Int
    func item(at index: Int) -> ProfileCellType
}

protocol ProfileInteractorOutputProtocol: AnyObject {
    func fetchDidSucceed()
    func didFail(with message: String)
}

protocol ProfileWorkerInterface {
    func fetch()
}

protocol ProfileWorkerOutputProtocol: AnyObject {
    func onSearchSuccess(_ response: ProfileResponse)
    func onError(_ error: APIError)
}

protocol ProfileRouterInterface {
    func showAlert(message: String)
}

protocol ProfilePresenterInterface {
    var view: ProfileViewInterface? { get set }
    var interactor: ProfileInteractorInterface { get set }
    var router: ProfileRouterInterface { get set }
}
