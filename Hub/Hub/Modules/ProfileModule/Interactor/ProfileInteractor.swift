//
//  ProfileInteractor.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

class ProfileInteractor {

    weak var presenter: ProfileInteractorOutputProtocol!
    var worker: ProfileWorkerInterface!

    var cells = [ProfileCellType]()
}

// MARK: - ProfileInteractor Interface
extension ProfileInteractor: ProfileInteractorInterface {
    func fetch() {
        worker.fetch()
    }
    
    func numberOfItems() -> Int {
        cells.count
    }
    
    func item(at index: Int) -> ProfileCellType {
        cells[index]
    }
}

// MARK: - ProfileWorker Output
extension ProfileInteractor: ProfileWorkerOutputProtocol {
    func onSearchSuccess(_ response: ProfileResponse) {
        cells = [
            .header(vm: .init(imageUrl: response.avatarUrl, name: response.name ?? "-")),
            .info(vm: .init(title: "E-mail", description: response.email ?? "-")),
            .info(vm: .init(title: "Public Repos", description: String(response.publicRepos ?? 0))),
            .info(vm: .init(title: "Followers", description: String(response.followers ?? 0))),
            .info(vm: .init(title: "Followings", description: String(response.following ?? 0))),
            .info(vm: .init(title: "Location", description: response.location ?? "-"))
        ]
        presenter.fetchDidSucceed()
    }
    
    func onError(_ error: APIError) {
        presenter.didFail(with: error.message)
    }
}
