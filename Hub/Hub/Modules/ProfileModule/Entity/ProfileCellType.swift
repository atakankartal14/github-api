//
//  ProfileCellType.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

enum ProfileCellType {

    case header(vm: ProfileHeaderCellViewModel)
    case info(vm: ProfileInfoCellViewModel)
}

extension ProfileCellType: Equatable {
    static func == (lhs: ProfileCellType, rhs: ProfileCellType) -> Bool {
        switch (lhs, rhs) {
        case (let .header(vm1), let .header(vm2)):
            return vm1.name == vm2.name && vm1.imageUrl == vm2.imageUrl
        case (let .info(vm1), let .info(vm2)):
            return vm1.title == vm2.title && vm1.description == vm2.description
        default:
            return false
        }
    }
}
