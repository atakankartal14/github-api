//
//  ProfilePresenter.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

class ProfilePresenter: ProfilePresenterInterface {

    weak var view: ProfileViewInterface?
    var interactor: ProfileInteractorInterface
    var router: ProfileRouterInterface

    init(view: ProfileViewInterface, interactor: ProfileInteractorInterface, router: ProfileRouterInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

// MARK: - ProfileView Output
extension ProfilePresenter: ProfileViewOutputProtocol {
    func viewDidLoad() {
        view?.setTitle("Profile")
        view?.showLoading()
        interactor.fetch()
    }
    
    func numberOfItems() -> Int {
        interactor.numberOfItems()
    }
    
    func item(at index: Int) -> ProfileCellType {
        interactor.item(at: index)
    }
}

// MARK: - ProfileInteractor Output
extension ProfilePresenter: ProfileInteractorOutputProtocol {
    func fetchDidSucceed() {
        view?.hideLoading()
        view?.reload()
    }
    
    func didFail(with message: String) {
        view?.hideLoading()
        router.showAlert(message: message)
    }
}
