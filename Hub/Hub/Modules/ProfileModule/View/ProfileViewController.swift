//
//  ProfileViewController.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import UIKit
import SVProgressHUD

class ProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var presenter: ProfileViewOutputProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(cellType: ProfileHeaderCell.self)
        tableView.register(cellType: ProfileInfoCell.self)
        presenter.viewDidLoad()
    }
}

// MARK: - ProfileView Interface
extension ProfileViewController: ProfileViewInterface {
    func setTitle(_ text: String) {
        title = text
    }
    
    func reload() {
        tableView.reloadData()
    }
    
    func showLoading() {
        SVProgressHUD.show()
    }
    
    func hideLoading() {
        SVProgressHUD.dismiss()
    }
}

// MARK: - TableView Delegate & DataSource
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = presenter.item(at: indexPath.row)
        switch item {
        case .header(let vm):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ProfileHeaderCell.self)
            cell.configure(model: vm)
            return cell
        case .info(let vm):
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ProfileInfoCell.self)
            cell.configure(model: vm)
            return cell
        }
    }
}
