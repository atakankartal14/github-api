//
//  ProfileInfoCell.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import UIKit
import Reusable

class ProfileInfoCell: UITableViewCell, NibReusable {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    public func configure(model: ProfileInfoCellViewModel) {
        titleLabel.text = model.title
        descriptionLabel.text = model.description
    }

}
