//
//  ProfileInfoCellViewModel.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

struct ProfileInfoCellViewModel {

    let title: String
    let description: String
}
