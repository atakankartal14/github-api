//
//  ProfileHeaderCellViewModel.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import Foundation

struct ProfileHeaderCellViewModel {

    let imageUrl: URL?
    let name: String
}
