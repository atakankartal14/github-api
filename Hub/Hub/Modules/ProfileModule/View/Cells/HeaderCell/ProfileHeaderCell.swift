//
//  ProfileHeaderCell.swift
//  Hub
//
//  Created by Atakan Kartal on 5.09.2021.
//

import UIKit
import Reusable
import Kingfisher

class ProfileHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        avatarImageView.layer.cornerRadius = 50
    }

    public func configure(model: ProfileHeaderCellViewModel) {
        avatarImageView.kf.setImage(with: model.imageUrl)
        nameLabel.text = model.name
    }
}
