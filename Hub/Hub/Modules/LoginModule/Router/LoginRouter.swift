//
//  LoginRouter.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import UIKit

class LoginRouter {

    var navigationController: UINavigationController!

    static func create() -> UINavigationController {
        let view = LoginViewController()
        let interactor = LoginInteractor()
        let router = LoginRouter()
        let worker = LoginWorker()
        let presenter = LoginPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        interactor.worker = worker
        worker.interactor = interactor
        let navigation = UINavigationController(rootViewController: view)
        router.navigationController = navigation
        return navigation
    }
}

// MARK: - LoginRouter Interface
extension LoginRouter: LoginRouterInterface {
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .cancel, handler: nil))
        navigationController.present(alert, animated: true, completion: nil)
    }
    
    func showMainVC() {
        UIApplication.shared.show(SearchRouter.create())
    }
}
