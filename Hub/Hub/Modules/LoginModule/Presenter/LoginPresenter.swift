//
//  LoginPresenter.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

class LoginPresenter: LoginPresenterInterface {

    weak var view: LoginViewInterface?
    var interactor: LoginInteractorInterface!
    var router: LoginRouterInterface

    init(view: LoginViewInterface, interactor: LoginInteractorInterface, router: LoginRouterInterface) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - LoginView Output
extension LoginPresenter: LoginViewOutputProtocol {
    func viewDidLoad() {
        guard let request = interactor.getOAuthRequest() else {
            router.showAlert(message: "Couldn't get OAuth Request!")
            return
        }
        view?.loadWebView(request)
    }

    func getAccessToken(with authUrl: URL) {
        interactor.getAccessToken(with: authUrl)
    }
}

// MARK: - LoginInteractor Output
extension LoginPresenter: LoginInteractorOutputProtocol {
    func acceessTokenRetrieved() {
        view?.hideLoading()
        router.showMainVC()
    }

    func extractionAuthCodeSucceed() {
        view?.showLoading()
    }

    func acceessTokenFailed(with message: String) {
        view?.hideLoading()
        router.showAlert(message: message)
    }
}
