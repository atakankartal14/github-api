//
//  LoginViewController.swift
//  Hub
//
//  Created by Atakan Kartal on 3.09.2021.
//

import UIKit
import WebKit
import Moya
import SVProgressHUD

class LoginViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!

    var presenter: LoginViewOutputProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        webView.navigationDelegate = self
        presenter?.viewDidLoad()
    }
}

// MARK: - LoginView Interface
extension LoginViewController: LoginViewInterface {
    func loadWebView(_ request: URLRequest) {
        webView.load(request)
    }

    func showLoading() {
        SVProgressHUD.show()
    }

    func hideLoading() {
        SVProgressHUD.dismiss()
    }
}

// MARK: - WKNavigation Delegate
extension LoginViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            presenter?.getAccessToken(with: url)
        }
        decisionHandler(.allow)
    }
}
