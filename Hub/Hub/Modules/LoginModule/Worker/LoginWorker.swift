//
//  LoginWorker.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Moya

class LoginWorker {

    typealias Provider = MoyaProvider<OAuthTarget<API.E>>

    weak var interactor: LoginWorkerOutputProtocol?
    var provider: Provider

    init(provider: Provider = API.oauthProvider) {
        self.provider = provider
    }

    private func createAccessTokenRequest(_ authCode: String) -> AccessTokenRequest {
        .init(grantType: APIConstants.oauthGrantType,
              code: authCode,
              clientId: API.E.oauthClientId,
              clientSecret: API.E.oauthClientSecret)
    }
}

// MARK: - LoginWorker Interface
extension LoginWorker: LoginWorkerInterface {
    func getAccessToken(with authCode: String) {
        provider.request(.accessToken(createAccessTokenRequest(authCode)), responseType: AccessTokenResponse.self)
        { [weak self] result in
            switch result {
            case .failure(let error):
                self?.interactor?.onError(error)
            case .success(let response):
                if let token = response.accessToken {
                    TokenManager.shared.accessToken = token
                    self?.interactor?.onSuccess()
                } else {
                    self?.interactor?.onError(.init(message: "Couldn't retrieve access token"))
                }
            }
        }
    }
}
