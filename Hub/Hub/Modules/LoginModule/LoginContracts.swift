//
//  LoginContracts.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation

protocol LoginViewInterface: AnyObject {
    func loadWebView(_ request: URLRequest)
    func showLoading()
    func hideLoading()
}

protocol LoginViewOutputProtocol {
    func viewDidLoad()
    func getAccessToken(with authUrl: URL)
}

protocol LoginInteractorInterface {
    func getOAuthRequest() -> URLRequest?
    func getAccessToken(with authUrl: URL)
}

protocol LoginInteractorOutputProtocol: AnyObject {
    func acceessTokenRetrieved()
    func extractionAuthCodeSucceed()
    func acceessTokenFailed(with message: String)
}

protocol LoginWorkerInterface {
    func getAccessToken(with authCode: String)
}

protocol LoginWorkerOutputProtocol: AnyObject {
    func onSuccess()
    func onError(_ error: APIError)
}

protocol LoginRouterInterface {
    func showAlert(message: String)
    func showMainVC()
}

protocol LoginPresenterInterface {
    var view: LoginViewInterface? { get set }
    var interactor: LoginInteractorInterface! { get set }
    var router: LoginRouterInterface { get set }
}
