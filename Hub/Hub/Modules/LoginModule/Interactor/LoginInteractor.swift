//
//  LoginInteractor.swift
//  Hub
//
//  Created by Atakan Kartal on 4.09.2021.
//

import Foundation
import Moya

class LoginInteractor {

    weak var presenter: LoginInteractorOutputProtocol?
    var worker: LoginWorkerInterface?

    private func extractAuthCode(from url: URL) -> String? {
        if
            url.absoluteString.hasPrefix(APIConstants.oauthCallbackURL),
            let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
            let code = (components.queryItems ?? []).first(where: { $0.name == "code" })?.value {
            return code
        }
        return nil
    }
}

// MARK: - LoginInteractor Interface
extension LoginInteractor: LoginInteractorInterface{
    func getOAuthRequest() -> URLRequest? {
        let target = OAuthTarget<API.E>.authorize(.init(clientId: API.E.oauthClientId,
                                                          scope: APIConstants.oauthScope,
                                                          redirectURL: APIConstants.oauthCallbackURL,
                                                          state: UUID().uuidString))
        return try? MoyaProvider.defaultEndpointMapping(for: target).urlRequest()
    }

    func getAccessToken(with authUrl: URL) {
        guard let authCode = extractAuthCode(from: authUrl) else { return }
        presenter?.extractionAuthCodeSucceed()
        worker?.getAccessToken(with: authCode)
    }
}

// MARK: - LoginWorker Output
extension LoginInteractor: LoginWorkerOutputProtocol {
    func onSuccess() {
        presenter?.acceessTokenRetrieved()
    }

    func onError(_ error: APIError) {
        presenter?.acceessTokenFailed(with: error.message)
    }
}
